# Architecture Ref. Card 03

Architecture Ref. Card 03 is a Spring Boot Application with a MariaDB Database meant for students of the BBW school to test with.

## Requirements
### OS
- Ubuntu 22.04
- Windows 10/11

### Framework
- Python 3 or higher
- openjdk-11

### Prior Knowledge
A basic understanding of the usage of Bash or Powershell

## Installation on local device

Download project

```sh
git clone https://gitlab.com/bbwrl/m346-ref-card-03.git
cd m346-ref-card-03
```

Builden des Projektes mit Maven
```sh
$ mvn package
```

Die erstellte Datei kann nun direkt mit Java gestartet werden.
```sh
$ java -DDB_USERNAME="jokedbuser" -DDB_PASSWORD="123456" -jar target/architecture-refcard-03-0.0.1-SNAPSHOT.jar
```

Im Browser ist die App unter der URL http://localhost:8080 erreichbar.

## Usage
This Project hosts a simple webpage that isn't particularly interactable. It's nice to look at though :smile: